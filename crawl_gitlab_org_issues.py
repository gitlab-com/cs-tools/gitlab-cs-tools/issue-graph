#!/usr/bin/env python3

import json
import argparse
import asyncio
import aiohttp
import datetime
import tarfile
from GitLabAsync import GitLabAsync

async def get_issues(token, updated_since):
    issues = []
    filtered_issues = []
    headers = {'PRIVATE-TOKEN': token}
    async with aiohttp.ClientSession(headers = headers) as session:
        gl = GitLabAsync(token, session)
        project_query_url = "https://gitlab.com/api/v4/projects/gitlab-org%2Fgitlab"
            # add updated at here to only get recently updated issues
        issues = await gl.get_project_issues(project_query_url, updated_since)
        for issue in issues:
            if issue["confidential"] == False:
                filtered_issues.append(issue)
        return filtered_issues

async def get_links(token, updated_since, existing_issues):
    headers = {'PRIVATE-TOKEN': token}
    count = 0
    async with aiohttp.ClientSession(headers = headers) as session:
        gl = GitLabAsync(token, session)
        project_query_url = "https://gitlab.com/api/v4/projects/gitlab-org%2Fgitlab"
        for issue in existing_issues:
            count += 1
            updated_date = datetime.datetime.strptime(issue["updated_at"], '%Y-%m-%dT%H:%M:%S.%fZ')

            docrawl = False
            if updated_since is None:
                docrawl = True
            else:
                if updated_date >= updated_since:
                    docrawl = True

            if docrawl:
                print("Getting links for issue %s of %s" % (count, len(existing_issues)))
                links = await gl.get_issue_links(project_query_url, issue["iid"])
                link_iids = []
                for linked_issue in links:
                    if linked_issue["confidential"] == False and linked_issue["project_id"] == 278964:
                        link_iid = [linked_issue["iid"],linked_issue["link_type"]] 
                        link_iids.append(link_iid)
                issue["links"] = link_iids
    return existing_issues

def merge_issues(existing_issues, new_issues):
    merged_issues = []
    existing_map = {}
    new_map = {}
    for issue in existing_issues:
        existing_map[issue["id"]] = issue
    for issue in new_issues:
        new_map[issue["id"]] = issue
    existing_map.update(new_map)
    return list(existing_map.values())

parser = argparse.ArgumentParser(description='Create report for GitLab issues')
parser.add_argument('token', help='API token able to read the requested projects')
parser.add_argument('--links', help='Only get related issues', action="store_true")
parser.add_argument('--updated_since', help='Only parse issues updated since this date, yyyy-mm-dd',
        type=lambda s: datetime.datetime.strptime(s, '%Y-%m-%d'))
args = parser.parse_args()

updated_since = None
if args.updated_since:
    updated_since = str(args.updated_since).replace(" ","T") + "Z"

existing_issues = []
new_issues = []

try:
    with tarfile.open("gitlab_issues.tar.gz", "r:gz") as tar:
        tar.extractall()
except:
    pass

try:
    with open("gitlab_issues.json","r", encoding="utf-8") as issuefile:
        existing_issues = json.load(issuefile)
except:
    print("Could not load existing issues")

if not args.links:
    loop = asyncio.get_event_loop()
    new_issues = loop.run_until_complete(get_issues(args.token, updated_since))
    new_issues = merge_issues(existing_issues, new_issues)
else:
    loop = asyncio.get_event_loop()
    if updated_since:
        updated_since = datetime.datetime.strptime(updated_since, '%Y-%m-%dT%H:%M:%SZ')
    new_issues = loop.run_until_complete(get_links(args.token, updated_since, existing_issues))

with open("gitlab_issues.json","w", encoding="utf-8") as outfile:
    json.dump(new_issues, outfile, indent=2)

with tarfile.open("gitlab_issues.tar.gz", "w:gz") as tar:
    tar.add("gitlab_issues.json")

