import json
from datetime import datetime
import re
import tarfile

def get_references(issue, issues):
	links = []
	issue_regex = "https:\/\/gitlab\.com/gitlab-org/gitlab/issues/[0-9]+"
	if issue["description"]:
		issue_iter = re.finditer(issue_regex, issue["description"])
		parsed_issues = set()
		for issue_match in issue_iter:
			issue_url = issue_match.group()
			issue_iid = int(issue_url[issue_url.rfind("/")+1:])
			if issue_iid in issues:
				if issue_iid != issue["iid"] and issues[issue_iid]["confidential"] == False:
					parsed_issues.add(issue_iid)
		links = list(parsed_issues)
	if "links" in issue:
		for link in issue["links"]:
			if link[0] not in links:
				links.append(link[0])
	return links

def make_node(issue, grouplist, elgrapho_id):
	data = {}
	data["iid"] = int(issue["iid"])
	data["elgrapho_id"] = elgrapho_id
	data["name"] = issue["title"]
	data["group"] = ""
	data["group_label"] = "None"
	data["type"] = "Feature"
	data["links"] = issue["references"]
	data["neighbor_nodes"] = []
	for label in issue["labels"]:
		if label.startswith("group"):
			data["group"] = grouplist.index(label[label.rfind(":")+1:])
			data["group_label"] = label[label.rfind(":")+1:]
		if label.lower() == "bug":
			data["type"] = "Bug"
	data["url"] = issue["web_url"]
	return data

def make_edges(issue, node_id_map, debug = False):
	edges = []
	failures = set()
	if not issue["references"]:
		return None
	else:
		for link in issue["references"]:
			edge = {}
			try:
				edge["from"] = int(node_id_map[issue["iid"]])
				edge["to"] = int(node_id_map[link])
				edges.append(edge)
			except:
				print("Could not establish edge from %s to %s. Node may be missing." % (issue["iid"], link))
				failures.add(link)
				continue
	return edges, failures

def make_graph(issues, group):
	print("Making graph %s" % group)
	graph = {"nodes":[],"edges":[],"steps":20}
	nodes = []
	node_id_map = {}
	linked_issues = set()
	issue_map = {}
	for issue in issues:
		if issue["confidential"] == False:
			issue_map[issue["iid"]] = issue

	groups = set()
	for issue in issues:
		if issue["confidential"] == False:
			issue_group = "None"
			links = get_references(issue, issue_map)
			for label in issue["labels"]:
				if label.startswith("group"):
					issue_group = label[label.rfind(":")+1:]
					groups.add(issue_group)
			issue["references"] = links

			if len(links)>0 and len(links)<50 and (issue_group == group or group == "all"):
				linked_issues.add(int(issue["iid"]))
				for link in links:
					linked_issues.add(int(link))

	group_list = list(groups)

	for issue in issues:
		if issue["confidential"] == False and issue["iid"] in linked_issues and len(issue["references"])<100:
			#node
			node = make_node(issue, group_list, len(nodes))
			node_id_map[issue["iid"]] = len(nodes)
			nodes.append(node)
	graph["nodes"] = nodes

	for issue in issues:
		if issue["confidential"] == False and issue["iid"] in linked_issues and len(issue["references"])<100:
			edges = make_edges(issue, node_id_map)
			if edges:
				graph["edges"].extend(edges[0])
				for failure in edges[1]:
					graph["nodes"][node_id_map[issue["iid"]]]["links"].remove(failure)

	for edge in graph["edges"]:
		source_node = graph["nodes"][edge["from"]]
		target_node = graph["nodes"][edge["to"]]
		if edge["to"] not in source_node["neighbor_nodes"]:
			source_node["neighbor_nodes"].append(edge["to"])
		if edge["from"] not in target_node["neighbor_nodes"]:
			target_node["neighbor_nodes"].append(edge["from"])
	return graph

def filter_issues(issues, group):
	issue_map = {}
	linked_issues = set()
	group_issues = set()
	for issue in issues:
		if issue["confidential"] == False:
			issue_map[issue["iid"]] = issue

	for issue in issues:
		issue_group = "None"
		for label in issue["labels"]:
			if label.startswith("group"):
				issue_group = label[label.rfind(":")+1:]
		links = get_references(issue, issue_map)
		issue["references"] = links

		# add group issues and the issues they link to linked issues
		if len(links)>0 and len(links)<50 and issue_group == group:
			group_issues.add(int(issue["iid"]))
			for link in links:
				#print("Adding %s to linked issues" % str(link))
				linked_issues.add(int(link))
	# add issues linking to group issues
	for issue in issues:
		for link in issue["references"]:
			if link in group_issues:
				linked_issues.add(int(issue["iid"]))
	filtered_issues_set = linked_issues.union(group_issues)
	filtered_issues = []
	for issue in filtered_issues_set:
		filtered_issues.append(issue_map[issue])
	return filtered_issues

nodes = []
graph = {"nodes":[],"edges":[],"steps":20}

node_id_map = {}

with tarfile.open("gitlab_issues.tar.gz", "r:gz") as tar:
    tar.extractall()

with open("gitlab_issues.json","r") as issuefile:
	issues = json.load(issuefile)
	groups = ["all","memory","pipeline execution","threat insights","pipeline authoring","configure","global search","monitor","source code","code review","license","None","package","runner","product intelligence","project management","product planning","testing","database","dynamic analysis","sharding","container security","geo","certify","composition analysis","workspace","release","purchase","import","analyzer frontend","verify","integrations","compliance","foundations","access","activation","static analysis","editor","utilization","optimize","conversion","scalability","distribution","gitaly","incubation","not_owned","adoption","applied ml","group templates","vulnerability research","machine learning","expansion","gitter","pricing"]
	#groups = ["gitter"]
	for group in groups:
		group_issues = issues
		if group != "all":
			group_issues = filter_issues(issues, group)
		graph = make_graph(issues, group)
		filename = group.replace(" ","_")
		with open("graphs/" + filename + ".json", "w") as outfile:
			json.dump(graph, outfile, indent=2)