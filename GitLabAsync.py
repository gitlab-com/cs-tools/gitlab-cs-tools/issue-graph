import json
import time
import re
import urllib.parse

class GitLabAsync(object):

    gl_url = ""
    headers = {}
    token = ""
    session = None
    next_page = 1
    cur_page = 0

    def __init__(self, token, session, gitlab = "https://gitlab.com/api/v4/"):
        self.gl_url = gitlab
        self.token = token
        self.headers = {'PRIVATE-TOKEN': token}
        self.session = session

    # "links" is a special parameter for issue links that don't have a working pagination
    async def query(self, url, is_list = True, params = "", links = False):
        result = []
        max_retries = 10
        cur_retries = 0
        page = 1
        # you can actually query non-list GET queries with paging parameters and the API is ok with it
        query_params = "?per_page=100&%s" % params if params else "?per_page=100"
        while True:
            if not links:
                print("Crawling issue page %s" % page)
            async with self.session.get(url + query_params + "&page=%s" % page) as resp:
                try:
                    response = await resp.json()
                except:
                    print("Error: Could not parse response for %s" % url)
                    break
                if 200 <= resp.status < 300:
                    if is_list == False:
                        return response
                    if len(response) == 0:
                        break
                    elif len(response) > 0 and len(response) < 100:
                        result.extend(response)
                        break
                    else:
                        result.extend(response)
                        if links:
                            break
                        page += 1
                # if we get a transient error or get rate limited, try again with exponential back off
                elif resp.status == 429 or resp.status in [500, 502, 503, 504]:
                    if cur_retries < max_retries:
                        wait_time = 2 ** cur_retries * 0.1
                        if "Retry-After" in resp.headers:
                            wait_time = int(resp.headers["Retry-After"])
                        cur_retries += 1
                        time.sleep(wait_time)
                        continue
                elif resp.status == 401:
                    print("Error: Not authenticated")
                    break
                else:
                    print("Error: Call failed with %s" % resp.status)
                    break
        return result

    async def get_project_issues(self, project_query_url, updated_since):
        api_url = project_query_url + "/issues"
        if updated_since:
            issues = await self.query(api_url, params="updated_after=%s" % str(updated_since))
        else:
            issues = await self.query(api_url)
        return issues

    async def get_issue_links(self, project_query_url, issue_iid):
        api_url = project_query_url + "/issues/" + str(issue_iid) + "/links"
        links = await self.query(api_url, links = True)
        return links

    async def get_project_issue(self, project_id, issue_iid):
        api_url = "https://gitlab.com/api/v4/projects/" + project_id + "/issues/" + str(issue_iid)
        print("Getting issue data for %s" % api_url)
        issue = await self.query(api_url, is_list = False)
        return issue

    async def parse_description(self, issue, salesforce_id, gitlab_issue_map):
        issue_regex = "https:\/\/gitlab\.com/gitlab-org/[a-zA-Z0-9_\-\.]+(\/-)?\/issues/[0-9]+"

        issue_iter = re.finditer(issue_regex, issue["description"])
        parsed_issues = set()

        for issue_match in issue_iter:
            parsed_issues.add(issue_match.group())
            # could use issue_match.span() to get string position and search backwards for priority labels
        issue_objects = []
        for parsed_issue in parsed_issues:
            # accounting for some old issue links in collab projects.
            # may not be relevant going foward
            project_id = parsed_issue[len("https://gitlab.com")+1:parsed_issue.rfind("issues")-1] \
                .replace("/-","") \
                .replace("gitlab-ee","gitlab") \
                .replace("gitlab-ce","gitlab-foss") \
                .replace("/","%2F")

            parsed_issue_iid = parsed_issue[parsed_issue.rfind("/")+1:]
            try:
                issue_object = await self.get_project_issue(project_id, parsed_issue_iid)
                #handle moved issues
                if issue["moved_to_id"]:
                    if str(issue["moved_to_id"]) in gitlab_issue_map:
                        target_issue_iid = gitlab_issue_map[str(issue_object["moved_to_id"])]
                        target_issue_object =  await self.get_project_issue(278964, target_issue_iid)
                        issue_objects.append(target_issue_object)
                else:
                    issue_objects.append(issue_object)
            except:
                print("GitLab issue not found: %s for customer %s in issue %s" % (parsed_issue, salesforce_id, issue["web_url"]))
                continue
        return issue_objects

    async def extract_issue_requests(self, project_query_url, salesforce_id, issue, gitlab_issue_map):
        customer_issues = []
        gitlab_issues = []
        #print("Adding linked issues from %s" % issue["iid"])
        issue_links = await self.get_issue_links(project_query_url, issue["iid"])
        for linked_issue in issue_links:
            if "gitlab-org" in linked_issue["web_url"]:
                gitlab_issues.append(linked_issue)
        #print("Parsing description for %s" % issue["iid"])
        if issue["description"]:
            issues_in_text = await self.parse_description(issue, salesforce_id, gitlab_issue_map)
            if issues_in_text:
                gitlab_issues.extend(issues_in_text)
        for issue in gitlab_issues:
            #print(issue)
            if issue:
                customer_issue = {}
                customer_issue["customer"] = salesforce_id
                customer_issue["collab_project"] = project_query_url
                customer_issue["found_in"] = project_query_url + "/issues/" + str(issue["iid"])
                customer_issue["title"] = issue["title"]
                customer_issue["url"] = issue["web_url"] + "/"
                customer_issue["labels"] = issue["labels"]
                customer_issue["milestone"] = ""
                if issue["milestone"]:
                    customer_issue["milestone"] = issue["milestone"]["title"]
                customer_issue["updated_at"] = issue["updated_at"]
                customer_issue["created_at"] = issue["created_at"]
                customer_issue["closed_at"] = issue["closed_at"]
                customer_issue["state"] = issue["state"]
                customer_issue["merge_requests_count"] = issue["merge_requests_count"]
                customer_issue["upvotes"] = issue["upvotes"]
                customer_issue["user_notes_count"] = issue["user_notes_count"]
                customer_issues.append(customer_issue)
        return customer_issues