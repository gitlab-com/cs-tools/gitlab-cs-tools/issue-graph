# issue-graph

A graph browser for gitlab-org/gitlab issues.

Crawls all non-confidential issues in https://gitlab.com/gitlab-org/gitlab. Uses their references in the description and their related issues to create a graph visualization. Only issues that have links are shown, unlinked issues are not included.

Issues can be searched by plain text, group label (via group::) and issue IID (via #). Clicking an issue shows some basic data and the linked issues. Clicking a search result will highlight the node and show its linked issues.

A separate group filter can be used to only show issues of specific groups.

![](graph_all.png)